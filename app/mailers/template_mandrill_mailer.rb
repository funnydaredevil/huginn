#encoding utf-8
class TemplateMandrillMailer < MandrillMailer::TemplateMailer
  default from: 'support@megogo.net'

  def compose(params, recipient=nil)
    @params = params

    mandrill_mail(
      template: template,
      subject: subject,
      to: recipient || email,
      vars: {
        'USER_NAME' => name,
        'EXPIRE_DATE' => expire_date,
        'LOCALE' => locale,
        'CREDIT_CARD' => credit_card,
        'OPERATION_DATE' => operation_date,
        'OPERATION_TYPE' => operation_type,
        'USER_ID' => user_id,
        'SUMM' => summ,
        'PAYMENT_RECEIVER' => payment_receiver,
        'TRANSACTION_ID' => transaction_id,
        'COUNTRY' => country,
        'PAYMENT_SYSTEM' => payment_system,
        # 'IS_TRY_N_BUY' => is_try_n_buy,
        'SUBSCRIPTION_NAME' => subscription_name
      },
      important: false,
      inline_css: true,
     )
  end

  private

  def email
    @params[:data][:user][:email] if @params[:data] && @params[:data][:user]
  end

  def name
    @params[:data][:user][:name] if @params[:data] && @params[:data][:user]
  end

  def credit_card
    @params[:data][:card_mask] if @params[:data]
  end

  def operation_date
    I18n.localize(@params[:data][:date].to_datetime, format: '%e %B %Y', locale: locale) if @params[:data] && @params[:data][:date]
  end

  def operation_type
    @params[:data][:description] if @params[:data]
  end

  def payment_receiver
    I18n.t 'template.llc'
  end

  def user_id
    @params[:data][:user][:id] if @params[:data] && @params[:data][:user]
  end

  def summ
    (@params[:data][:amount] + ' ' + @params[:data][:currency]) if @params[:data]
  end

  def transaction_id
    @params[:data][:transaction_id] if @params[:data]
  end

  def country
    @params[:data][:geo] if @params[:data]
  end

  def payment_system
    I18n.t 'template.payment_system'
    # case @params[:data][:payment_system]
    # when 'privat'
    #   'Приват Банк'
    # end
  end

  def template
    if @params[:data] && @params[:data][:subscription]
      case @params[:data][:subscription][:product]
      when 'main'
        t = 'autoprolong-main'
      when 'tv'
        t = if @params[:data][:subscription][:is_try_n_buy]
          'autoprolong-tv-try-buy'
        else
          'autoprolong-tv'
        end
      end
    else
      t = @params[:message]
    end
    t ||= 'charge_card'
    t.prepend('no-') if @params[:data][:is_auto_prolong] && @params[:data][:is_auto_prolong] == false
    t.prepend("#{locale}-") unless locale == 'ru'
    t
  end

  def subject
    if @params[:data] && @params[:data][:description].present?
      @params[:data][:description]
    else
      I18n.t('template.subscription_expires') + subscription_name
    end
  end

  def expire_date
    date = @params[:data][:date].to_datetime if @params[:data] && @params[:data][:date]
    today_or_tomorrow = if date && date.today?
      I18n.t 'template.today'
    elsif date && (date - 1.day).today?
      I18n.t 'template.tomorrow'
    else
      ''  
    end
    today_or_tomorrow + ' ' + I18n.localize(date, format: '%e %B %Y', locale: locale) if date
  end
  
  def locale
    locale = if @params[:data][:geo] == 'cz' || @params[:data][:geo] == 'c2'
      'cz'
    else
      'ru'
    end
    I18n.locale = locale
    locale
  end

  def subscription_name
    @params[:data][:subscription][:name] if @params[:data] && @params[:data][:subscription]
  end
end