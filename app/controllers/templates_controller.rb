class TemplatesController < ApplicationController
  include SortableTable
  require 'mandrill'
  before_action :mandrill
  before_action :fetch_template, only: [:show, :edit, :update]

  def index
    set_table_sort sorts: %w[slug name], default: { name: :asc }

    @templates = @mandrill.templates.list.map { |t| OpenStruct.new t }
  end

  def show
  end

  def new
  end

  def create
    begin
      @template = mandrill.templates.add(params[:template][:name], nil, nil, nil, params[:template][:code], nil, nil, []) if params[:template]
      redirect_to templates_path
    rescue Mandrill::Error => e
      params[:error] = "A mandrill error occurred: #{e.class} - #{e.message}"
      redirect_to new_template_path(params)
    end
  end

  def edit
  end

  def update
    begin
      if params[:template]
        mandrill.templates.update(@template.name, nil, nil, nil, params[:template][:code], nil, nil, [])
        @template = mandrill.templates.publish @template.name
      end
      redirect_to templates_path
    rescue Mandrill::Error => e
      params[:error] = "A mandrill error occurred: #{e.class} - #{e.message}"
      redirect_to edit_template_path(params)
    end
  end

  def destroy
    begin
      @template = mandrill.templates.delete params[:id]
    rescue Mandrill::Error => e
    end
    redirect_to templates_path
  end

  def publish
    begin
      @template = mandrill.templates.publish params[:template_id]
    rescue Mandrill::Error => e
    end
    redirect_to templates_path
  end

  private

  def mandrill
    @mandrill = Mandrill::API.new(ENV['MANDRILL_APIKEY'])
  end

  def fetch_template
    @template = OpenStruct.new(@mandrill.templates.info(params[:id]))
  end
end