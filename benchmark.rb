require 'httparty'

class Poster
  include HTTParty

  def post
    options = { :body => {:payload => {:email => 'trololo@example.com', :balance => Random.rand(11) } } }
    self.class.post('http://localhost:3005/users/1/web_requests/12/supersecretstring', options)
  end

end

webhook = Poster.new

2.times do
  webhook.post
end